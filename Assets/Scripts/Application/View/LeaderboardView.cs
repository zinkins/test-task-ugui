﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GameVision
{
    public class LeaderboardView : View<GameVisionApplication>
    {
        public Transform userRankPrefab;
        public Transform spawnPoint;
        
        List<Transform> _currentUserRanks;
        
        public void DrawData(List<User> userRanks)
        {
            if (userRanks == null)
            {
                return;
            }

            if (_currentUserRanks != null && _currentUserRanks.Count != 0)
            {
                for (int i = 0; i < userRanks.Count; i++)
                {
                    if (i < _currentUserRanks.Count)
                    {
                        if (!_currentUserRanks[i].gameObject.activeSelf)
                        {
                            _currentUserRanks[i].gameObject.SetActive(true);
                        }
                        UserRankView userRank = _currentUserRanks[i].GetComponent<UserRankView>();
                        userRank.SetData(userRanks[i], i+1);
                    } else
                    {
                        Transform t = Instantiate(userRankPrefab);
                        t.SetParent(spawnPoint);
                        UserRankView userRank = t.GetComponent<UserRankView>();
                        userRank.SetData(userRanks[i], i + 1);

                        t.localScale = Vector2.one;
                        t.SetAsLastSibling();
                        _currentUserRanks.Add(t);
                    }
                }
                // userRanks contains less elements than old _currentUserRanks
                // disable it
                for (int i = userRanks.Count; i < _currentUserRanks.Count; i++)
                {
                    _currentUserRanks[i].gameObject.SetActive(false);
                }
            } else
            {
                _currentUserRanks = new List<Transform>();
                for (int i = 0; i < userRanks.Count; i++)
                {
                    Transform t = Instantiate(userRankPrefab);
                    t.SetParent(spawnPoint);
                    UserRankView userRank = t.GetComponent<UserRankView>();
                    userRank.SetData(userRanks[i], i + 1);

                    t.localScale = Vector2.one;
                    t.SetAsLastSibling();
                    _currentUserRanks.Add(t);
                }
            }

            //transform.parent.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, (height + betweenSize) * userRanks.Count);
            //transform.parent.localPosition = new Vector3(0, -(height + betweenSize) * userRanks.Count, 0);
            

        }
    }
}