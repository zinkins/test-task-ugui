﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

namespace GameVision
{
    public class UserRankView : View<GameVisionApplication>
    {
        public User userRankInfo;

        public Text rankText;
        public Text scoreText;
        public Text usernameText;

        public void SetData(User userRank, int rank)
        {
            rankText.text = rank.ToString();
            scoreText.text = userRank.score.ToString();
            usernameText.text = userRank.userFirstName + userRank.userLastName;
        }
    }
}