﻿using UnityEngine;
using System.Collections;

namespace GameVision
{
    public class GameVisionView : View<GameVisionApplication>
    {
        public LeaderboardView leaderboardView { get { return mLeaderboardView = Assert<LeaderboardView>(mLeaderboardView); } }
        private LeaderboardView mLeaderboardView;

        public RankScreenView rankView { get { return mRankView = Assert(mRankView); } }
        private RankScreenView mRankView;

    }
}