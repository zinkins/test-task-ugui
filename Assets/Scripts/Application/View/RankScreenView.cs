﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

namespace GameVision
{
    public class RankScreenView : View<GameVisionApplication>
    {
        public User userRankInfo;
        public Image avatarBack;
        public Image avatar;

        public Text rankText;
        public Text scoreText;
        public Text usernameText;
        public Text betterThanPercentsText;
        public Text betterThanText;
        public Transform betterThanLine;
        public int graphStart;
        public int graphEnd;

        public void SetData(User user, float percents)
        {
            avatar.sprite = Resources.Load<Sprite>("Avatars/" + app.model.avatarBank.avatars[user.avatarID].avatar);
            avatarBack.sprite = Resources.Load<Sprite>("Avatars/" + app.model.avatarBank.avatars[user.avatarID].avatarBack);

            rankText.text = user.rank.ToString();
            scoreText.text = user.score.ToString();
            usernameText.text = user.userFirstName + user.userLastName;

            betterThanPercentsText.text = percents.ToString("##0") + "%";
            betterThanText.text = "you are better than " + percents.ToString("##0") + "%";
            betterThanLine.localPosition = new Vector3(graphStart + (graphEnd - graphStart) * percents / 100f,
                betterThanLine.localPosition.y,
                betterThanLine.localPosition.z);
        }
    }
}