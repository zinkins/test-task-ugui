﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace GameVision
{
    public class RankScreenController : Controller<GameVisionApplication>
    {
        public override void OnNotification(string p_event, Object p_target, params object[] p_data)
        {
            switch (p_event)
            {
                case EventConsts.DIFFICULTY_EASY:
                    app.model.currentDifficultyFilter = FILTER_DIFFICULTY.EASY;
                    DrawRankScreen();
                    Log(" Draw easy players ");
                    break;
                case EventConsts.DIFFICULTY_MEDIUM:
                    app.model.currentDifficultyFilter = FILTER_DIFFICULTY.MEDIUM;
                    DrawRankScreen();
                    Log(" Draw medium players ");
                    break;
                case EventConsts.DIFFICULTY_HARD:
                    app.model.currentDifficultyFilter = FILTER_DIFFICULTY.HARD;
                    DrawRankScreen();
                    Log(" Draw hard players ");
                    break;
                case EventConsts.STATUS_FILTER_ALL:
                    app.model.currentStatusFilter = STATUS.ALL_STATUSES;
                    DrawRankScreen();
                    Log(" Draw ALL players ");
                    break;
                case EventConsts.STATUS_FILTER_ROOKIE_ONLY:
                    app.model.currentStatusFilter = STATUS.ROOKIE;
                    DrawRankScreen();
                    Log(" Draw ROOKIE players ");
                    break;

                case EventConsts.GOT_RANK_DATA:
                    DrawRankScreen();
                    Log(" Draw Leaderboard");
                    break;
            }
        }
        
        void DrawRankScreen()
        {
            IEnumerable<User> list;
            if (app.model.currentStatusFilter == STATUS.ALL_STATUSES)
            {
                list = (from t in app.model.userRanks
                        where t.difficulty == app.model.currentDifficultyFilter
                        orderby t.rank
                        select t
                );

            }
            else
            {
                list = (from t in app.model.userRanks
                        where t.difficulty == app.model.currentDifficultyFilter
                        && t.status == app.model.currentStatusFilter
                        orderby t.rank
                        select t
                );
            }

            app.model.user.rank = (
                from t in list
                where t.score > app.model.user.score
                select t
                ).Count() + 1;

            float percents = 0f;
            if (list.Count() != 0)
            {
                percents = (float) (
                    from t in list
                    where t.score <= app.model.user.score
                    select t
                    ).Count() / list.Count() * 100f;
            } else
            {
                percents = 100f;
            }
            // add our user to leaderboard
            List<User> users = list.Union(new List<User>() { app.model.user }).OrderByDescending(u => u.score).Take(10).ToList();
            app.view.rankView.SetData(app.model.user, percents);
            app.view.leaderboardView.DrawData(users);
        }
    }
}