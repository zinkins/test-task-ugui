﻿using UnityEngine;
using System.Collections;

namespace GameVision
{
    public class BackButtonController : Controller<GameVisionApplication>
    {

        public override void OnNotification(string p_event, Object p_target, params object[] p_data)
        {
            switch (p_event)
            {
                case EventConsts.BACK_BUTTON:
                    Log(" Show previous screen ");
                    break;
            }
        }

    }
}
