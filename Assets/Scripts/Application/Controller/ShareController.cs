﻿using UnityEngine;
using System.Collections;

namespace GameVision
{
    public class ShareController : Controller<GameVisionApplication>
    {

        public override void OnNotification(string p_event, Object p_target, params object[] p_data)
        {
            switch (p_event)
            {
                case EventConsts.SHARE_BUTTON:
                    Log(" Show Share popup ");
                    break;
            }
        }

    }
}
