﻿using UnityEngine;
using System.Collections;

namespace GameVision
{
    public enum STATUS
    {
        ALL_STATUSES = 0,
        ROOKIE = 1,
        SOME_OTHER_STATUS = 2
    }

    public enum FILTER_DIFFICULTY
    {
        EASY = 0,
        MEDIUM = 1,
        HARD = 2
    }

    public class EventConsts
    {
        public const string FB_INVITE_FRIENDS = "facebook.invite_friends@down";
        public const string DIFFICULTY_EASY = "difficulty.easy@down";
        public const string DIFFICULTY_MEDIUM = "difficulty.medium@down";
        public const string DIFFICULTY_HARD = "difficulty.hard@down";
        public const string STATUS_FILTER_ALL = "status_filter.all@down";
        public const string STATUS_FILTER_ROOKIE_ONLY = "status_filter.rookie_only@down";
        public const string GOT_RANK_DATA = "got_rank_data";
        public const string BACK_BUTTON = "button_back@down";
        public const string SHARE_BUTTON = "button_share@down"; 
    }
}