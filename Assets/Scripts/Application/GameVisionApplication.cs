﻿using UnityEngine;
using System.Collections.Generic;

namespace GameVision
{
    public class GameVisionApplication : BaseApplication<GameVisionModel, GameVisionView, GameVisionController>
    {
        void Start()
        {
            model.currentStatusFilter = STATUS.ALL_STATUSES;
            model.currentDifficultyFilter = FILTER_DIFFICULTY.EASY;

            Init();

        }


        void Init()
        {
            GenerateFakeUserData();
            GenerateFakeRankData();
            Notify(EventConsts.GOT_RANK_DATA);
        }

        void GenerateFakeUserData()
        {
            model.user = new User() {
                avatarID = Random.Range(0, model.avatarBank.Length-1),
                userFirstName = RandomWordString(1),
                userLastName = RandomWordString(1),
                userID = 0,
                score = Random.Range(2000, 10000),
                status = (STATUS)Random.Range(1, System.Enum.GetValues(typeof(STATUS)).Length),
                difficulty = RandomEnumValue<FILTER_DIFFICULTY>()
            };
        }

        void GenerateFakeRankData()
        {
            model.userRanks = new List<User>();
            int tempScore = Random.Range(2000, 10000);

            for (int i = 0; i < 50; i++)
            {
                model.userRanks.Add(
                    new User()
                    {
                        userFirstName = RandomWordString(1),
                        userLastName = RandomWordString(1),
                        userID = i + 1,
                        score = tempScore,
                        // ALL_STATUSES == 0 not included
                        status = (STATUS) Random.Range(1, System.Enum.GetValues(typeof(STATUS)).Length),
                        difficulty = RandomEnumValue<FILTER_DIFFICULTY>()
                    }
                );
                tempScore = Random.Range(tempScore-100, tempScore);
            }
        }

        static string RandomWordString(int length)
        {
            var wordArray = new string[7] { "John", "Smith", "Paul", "Phillips", "Jill", "Bill", "Doe" };
            string returnString = string.Empty;
            for (int i = 0; i < length; i++)
            {
                returnString += wordArray[Random.Range(0, wordArray.Length)] + " ";
            }
            returnString.TrimEnd();
            return returnString;
        }

        static T RandomEnumValue<T>()
        {
            var v = System.Enum.GetValues(typeof(T));
            return (T)v.GetValue(Random.Range(0, v.Length));
        }
    }
}
