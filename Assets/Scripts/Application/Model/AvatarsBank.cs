﻿using UnityEngine;
using System.Collections.Generic;

namespace GameVision
{
    public class AvatarsBank : Model<GameVisionApplication>
    {
        public Avatar[] avatars;

        public int Length
        {
            get
            {
                return avatars.Length;
            }
        }
    }

}