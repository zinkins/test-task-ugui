﻿using UnityEngine;
using System.Collections.Generic;

namespace GameVision
{
    public class GameVisionModel : Model<GameVisionApplication>
    {
        public STATUS currentStatusFilter;
        public FILTER_DIFFICULTY currentDifficultyFilter;

        public User user;

        public List<User> userRanks;

        public AvatarsBank avatarBank { get { return mAvatars = Assert<AvatarsBank>(mAvatars); } }
        private AvatarsBank mAvatars;
        
    }
}
