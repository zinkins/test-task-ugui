﻿using UnityEngine;
using System.Collections;

namespace GameVision
{
    public class User
    {
        public int userID;
        public string userFirstName;
        public string userLastName;
        public int rank;
        public int score;
        public FILTER_DIFFICULTY difficulty;
        public STATUS status;

        public int avatarID;
    }

}