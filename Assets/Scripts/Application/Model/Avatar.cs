﻿using UnityEngine;
using System.Collections;

namespace GameVision
{
    [System.Serializable]
    public class Avatar 
    {
        public int id;
        public string avatar;
        public string avatarBack;
    }

}